from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.core.exceptions import MultipleObjectsReturned
from .models import SignUp
from .forms import SignUp_Form

EMAIL_HAS_BEEN_USED = 'E-mail has been used'

# Create your views here.
def signup(request):
	events = SignUp.objects.all().values()

	if request.method == "POST":
		try:
			SignUp.objects.get(email=request.POST['email'])
			return HttpResponseServerError(EMAIL_HAS_BEEN_USED)
		except SignUp.DoesNotExist:
			pass
		except MultipleObjectsReturned:
			#return SignUp.objects.filter(email=request.POST['email']).first()
			return HttpResponseServerError(EMAIL_HAS_BEEN_USED)
		form = SignUp_Form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('sign_up:signup'))

	else:
		form = SignUp_Form()

	response = {'bang': form, 'events': events}
	html = 'signUp.html'
	return render(request, html, response)

'''def signup_fix(request):
    events = {'bang':forms, 'events':events}
    if request.method == 'POST':
        try:
            SignUp.objects.get(email=request.POST['email'])
            return HttpResponseServerError(EMAIL_HAS_BEEN_USED)
        except SignUp.DoesNotExist:
            pass
        newSignUp = SignUp(
            name=request.POST['name'],
            tanggal_lahir=dateutil.parser.parse(request.POST['tanggal_lahir']),
            email=request.POST['email'],
            password=request.POST['password']
        )
        newSignUp.save()

    return render(request, 'signUp.html', events)'''



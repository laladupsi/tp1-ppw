from django.test import TestCase, Client
from django.http import HttpRequest

from .views import signup
from .models import SignUp
from .forms import SignUp_Form
import unittest
# Create your tests here.

class RegistationsTest(TestCase):
	
	def text_index_url(self):
		response = Client().get("/signup/")
		self.assertEqual(response.status_code, 200)

	'''def test_form_title(self):
		request = HttpRequest()
		response = signup(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Sign Up', html_response)

	def test_button(self):
		request = HttpRequest()
		response = signup(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Submit', html_response)

	def test_signupHtml_is_not_empty(self):
		create_register = SignUp.objects.create(nama = "Sar Syifa", tanggal_lahir = "1999-31-12", email = "sarsyifa@outlook.com", password = "capek")
		response = Client().get("/signup/")
		html_response = response.content.decode('utf8')
		self.assertIn("tanggal_lahir", html_response)
	 	
	def test_create_model(self):
		create_register = SignUp.objects.create(nama = "Sar Syifa", tanggal_lahir = "1999-31-12", email = "sarsyifa@outlook.com", password = "capek")
		create_register.save()
		count_register = SignUp.objects.all().count()
		self.assertEqual(count_register, 1)'''

	def test_model_exist(self):
		SignUp.objects.create(nama = "Sar Syifa", tanggal_lahir = "1999-12-31", email = "sarsyifa@outlook.com", password = "capek")
		count_register = SignUp.objects.all().count()
		self.assertEqual(count_register, 1)

	def test_form_already(self):
		form = SignUp_Form()
		self.assertIn("nama", form.as_p())
		self.assertIn("tanggal_lahir", form.as_p())
		self.assertIn("email", form.as_p())
		self.assertIn("password", form.as_p())

	def test_form_empty_input(self):
		data = {'email' : ''}
		form = SignUp_Form(data)
		self.assertFalse(form.is_valid())

	def test_form_to_model(self):
		request = HttpRequest()
		request.method = "POST"
		request.POST['name'] = "abang"
		request.POST['tanggal_lahir'] = "2018-10-18"
		request.POST['email'] = "sarsyifa@yahoo.com"
		request.POST['password'] = "testhehe"


	'''def test_can_signup_from_input_form(self):
		self.client.post("/signup/", {"nama" : "abang", "tanggal_lahir" : "2018-10-18", "email"  : "sarsyifa@yahoo.com", "password" : "testhehe"})
		count_register = SignUp.objects.all().count()
		self.assertEqual(count_register, 1)

	def test_email_has_been_used(self):
		self.client.post("/signup/", {"nama" : "abang", "tanggal_lahir" : "2018-10-18", "email" : "sarsyifa@yahoo.com", "password" : "testhehe"})
		count_register = SignUp.objects.all().count()
		self.assertEqual(count_register, 1)

		response = self.client.post("/signup/", {"nama" : "abang", "tanggal_lahir" : "2018-10-18", "email" : "sarsyifa@yahoo.com", "password" : "testhehe"})
		count_register = SignUp.objects.all().count()
		self.assertEqual(count_register, 1)

		html_response = response.content.decode('utf8')
		self.assertIn("email has been used", html_response)'''


from django import forms
from .models import SignUp
from django.forms import ModelForm

class SignUp_Form(forms.ModelForm):

	password = forms.CharField(widget=forms.PasswordInput)
	class Meta:
		model = SignUp
		fields = ['nama', 'tanggal_lahir', 'email', 'password']
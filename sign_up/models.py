from django.db import models
from django.utils import timezone
from datetime import date

# Create your models here.
class SignUp(models.Model):
	nama = models.CharField(max_length=50)
	tanggal_lahir = models.DateField(default=timezone.now)
	email = models.EmailField(max_length=30, unique=True)
	password = models.CharField(max_length=30)
from django.contrib import admin
from django.urls import include,path
from django.conf.urls import url
from .views import signup

urlpatterns = [
    path('', signup, name='signup'),
]
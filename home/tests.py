from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

from selenium import webdriver
import unittest

class HomeTest(object):
	"""docstring for HomeTest"""
	def test_url_is_active(self, arg):
		response = Client().get('/homepage/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_index():
		found = resolve('/homepage/')
		self.assertEqual(found.func, index)


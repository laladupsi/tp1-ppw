from django.contrib import admin
from .models import Project

# Register your models here.        
class ProjectModelAdmin(admin.ModelAdmin):
    list_display = ["title", "custom_category"]
    list_filter = ["custom_category"]
    
    search_fields = ["title", "custom_category"]
    
    class Meta:
        model = Project
    
admin.site.register(Project, ProjectModelAdmin)
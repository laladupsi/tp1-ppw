from django.db import models
import os, uuid

# Create your models here.
def project_image_path(instance, filename):
    return os.path.join('project', filename)
    
class Project(models.Model):
    title = models.CharField(max_length = 250)
    text = models.TextField()
    project_image = models.ImageField(upload_to=project_image_path, blank=True, null=True)
    current_donation = models.IntegerField(default=0)
    max_donation = models.IntegerField(default=100)
    
    custom_category = models.CharField(max_length = 250)
    
    @property
    def percentage(self):
        return int(self.current_donation/self.max_donation*100)
    
    def get_image(self):
        if(not self.project_image):
            return 'https://vignette.wikia.nocookie.net/citrus/images/6/60/No_Image_Available.png/revision/latest?cb=20170129011325'
        return self.project_image.upload_to
    
    def __str__(self):
        return self.custom_category+" T>"+self.title
"""tp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from donatepage import urls
import home.urls as home
import sign_up.urls as sign_up
import program_detail.urls as program_detail
from News.views import news, generate_article_page
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('home.urls','home'),namespace='home')),
    path('donate/', include(('donatepage.urls', 'donate'), namespace='donate')),
    path('News/', include(('News.urls','news'),namespace='news')),
    path('signup/', include(('sign_up.urls', 'sign_up'), namespace='signup')),
    path('program/', include(('program_detail.urls','program_detail'), namespace='program_detail'))
]

urlpatterns += staticfiles_urlpatterns()
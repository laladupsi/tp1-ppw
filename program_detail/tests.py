from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class ProgramDetailUnitTest(TestCase):
    def test_page_url_is_exist(self):
        response = Client().get('/program/') #this should be unique program id appended from db
        self.assertEqual(response.status_code,200)

    def test_page_using_index_func(self):
        found = resolve('/program/')
        self.assertEqual(found.func,index)

    def test_page_using_template(self):
        response = Client().get('/program/')
        self.assertTemplateUsed(response,'program_detail.html')
    
    def test_page_render_donate_result(self):
        test = 'Anonymous'
        response = Client().get('/program/')
        html_response  = response.content.decode('utf8')
        self.assertIn(test,html_response)

from django.contrib import admin
from .models import MainArticle, NewsPageSetting

# Register your models here.
class MainArticleModelAdmin(admin.ModelAdmin):
    list_display = ["title", "date", "custom_id", "id"]
    list_filter = ["date", "custom_id"]
    
    search_fields = ["title", "text"]
    
    class Meta:
        model = MainArticle
    
admin.site.register(MainArticle, MainArticleModelAdmin)
admin.site.register(NewsPageSetting)
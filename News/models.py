from django.db import models
from project.models import Project
from django.core.validators import MinValueValidator
import os, uuid

# Create your models here.
def article_image_path(instance, filename):
    return os.path.join('article', filename)

class NewsPageSetting(models.Model):
    top_news_count = models.IntegerField(default=3)
    
class MainArticle(models.Model):
    author = models.CharField(max_length = 100, default = 'anon')
    id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True, primary_key=True)
    title = models.CharField(max_length = 250)
    category = models.CharField(max_length = 50)   
    date = models.DateField(auto_now = False, auto_now_add = True)
    article_image = models.ImageField(upload_to=article_image_path, blank=True, null=True)
    text = models.TextField(default="<p></p>")
    custom_id = models.IntegerField(default=1, validators=[MinValueValidator(1)])
    
    custom_category = models.CharField(max_length = 250)
    projects = models.ManyToManyField(Project, blank=True, null=True)
    
    @property
    def get_image(self):
        if(not self.article_image):
            return 'https://vignette.wikia.nocookie.net/citrus/images/6/60/No_Image_Available.png/revision/latest?cb=20170129011325'
        return self.article_image.url
    
    def __str__(self):
        return self.title
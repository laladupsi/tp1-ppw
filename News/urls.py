from django.urls import path

from django.contrib.staticfiles.urls import staticfiles_urlpatterns


from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import news, generate_article_page
from News import views

urlpatterns = [
    url(r'^$', news, name='index'),
    url(r'^generate_article_page/(?P<pk>[0-9a-f-]+)/$', generate_article_page, name='generate_article_page'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
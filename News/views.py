from django.shortcuts import render
from django.http import HttpResponseNotFound
from .models import MainArticle, NewsPageSetting, Project
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

def news(request):
    all_article = MainArticle.objects.order_by('custom_id').all()
    
    try:
        top_article_count = NewsPageSetting.objects.first().top_news_count
    except AttributeError:
        top_article_count = 1
        
    top_article_first = all_article.first()
    top_article = all_article[1:top_article_count]
    top_article_range = top_article.count()+1
    
    all_article = all_article[top_article_range:]
    
    return render(request, 'news.html', {'all_article':all_article,
                                          'top_article':top_article,
                                          'first_article':top_article_first,
                                          'top_article_range':range(1,top_article_range)})

def generate_article_page(request, pk):
    template = 'article.html'
    
    try:
        article = MainArticle.objects.get(pk=pk)
    except MainArticle.DoesNotExist:
        HttpResponseNotFound("page not found")
        
    all_project = article.projects.all()
    
    if(all_project.count() <= 4):
        return render(request, template, {'nbar': 'article', 
                                          'article':article,
                                          'unmoving_projects':all_project,
                                          'carousel_display':'d-none'})
    
    first_project = all_project.first()
    all_project = all_project[1:]
    
    return render(request, template, {'nbar': 'article', 
                                      'article':article,
                                      'first_project':first_project,
                                      'projects':all_project})
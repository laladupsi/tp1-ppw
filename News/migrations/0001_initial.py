# Generated by Django 2.1.2 on 2018-10-17 12:33

import News.models
import django.core.validators
from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('project', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainArticle',
            fields=[
                ('author', models.CharField(default='anon', max_length=100)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('title', models.CharField(max_length=250)),
                ('category', models.CharField(max_length=50)),
                ('date', models.DateField(auto_now_add=True)),
                ('article_image', models.ImageField(blank=True, null=True, upload_to=News.models.article_image_path)),
                ('text', models.TextField(default='<p></p>')),
                ('custom_id', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)])),
                ('custom_category', models.CharField(max_length=250)),
                ('projects', models.ManyToManyField(blank=True, to='project.Project')),
            ],
        ),
        migrations.CreateModel(
            name='NewsPageSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('top_news_count', models.IntegerField(default=3)),
            ],
        ),
    ]

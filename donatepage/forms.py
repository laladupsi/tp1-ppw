from django import forms
from .models import DonateModel

class DonateForm(forms.ModelForm):
    class Meta:
        model = DonateModel
        fields = ('name','email','nominal','password','message','checkbox',)

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Your name'}),
            'email': forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Email'}),
            'nominal': forms.NumberInput(attrs={'class': 'form-control','placeholder':'Amount of donation'}),
            'password': forms.PasswordInput(attrs={'class':'form-control','type':'password','placeholder':'Password'}),
            'message': forms.Textarea(attrs={'class': 'form-control','type': 'text','placeholder':'Write something nice','rows':'3','cols':'10'}),
            'checkbox': forms.CheckboxInput(),
        }
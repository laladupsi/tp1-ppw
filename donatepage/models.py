from django.db import models
from datetime import datetime, date
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


# Create your models here.
def validate_positive(value):
    if value < 0 and value % 1 != 0 :
        raise ValidationError(
            _('%(value)s is not positive integer number'),
            params={'value': value},
        ) 

class DonateModel(models.Model):
    name = models.CharField(max_length = 3000)
    email = models.EmailField(max_length = 3000)
    nominal = models.IntegerField(validators=[validate_positive])
    message = models.TextField(max_length = 3000)
    password = models.CharField(max_length = 3000)
    checkbox = models.BooleanField()

    def ___str__(self):
        return self.name

    

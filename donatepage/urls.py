from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls import url
from .views import index_donate_confirm, show_form
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = "Donate"
urlpatterns = [
    path('', show_form, name='donate'),
    path('addDonasi', index_donate_confirm, name='adddonasi'),
    url(r'^addDonasi',index_donate_confirm, name='addDonasi'),
]

urlpatterns += staticfiles_urlpatterns()
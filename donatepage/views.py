from django.shortcuts import render
from .forms import DonateForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import DonateModel
from project.models import Project

# Create your views here.
response = {}

def show_form(request):
    donasi = DonateModel.objects.all()
    form = DonateForm()
    html = 'donate_page.html'
    return render(request, html, {'donasi':donasi, 'form':form})

def index_donate_confirm(request, tittle):
    project = Project.objects.get(pk=pk)
    if request.method == 'POST':
        form = DonateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('Donate:donate'))
        else:
            form = DonateForm()
            return render(request, 'donate_page.html', {'form' : form})
    
    form = DonateForm()
    return render(request, 'donate_page.html', {'form':form, 'project':project})

def index_donate_cancel(request):
    return render(request, 'index.html')
